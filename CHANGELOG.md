# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog],
and this project adheres to [Semantic Versioning].

## [Unreleased]

## [0.3.0]

Released on 2024-01-01.

### Changed

- `Token::new` is not `const`.
- Update the minimum supported Rust version to 1.61.

### Removed

- `GhostCell` and `GhostToken`.

### Fixed

- Update to `type-factory` 0.3 because 0.1 is [unsound](https://codeberg.org/wackbyte/type-factory/issues/2).

## [0.2.1]

Released on 2023-12-30.

Yanked on 2024-01-01.

### Changed

- Rename `GhostCell` and `GhostToken` to `Cell` and `Token`. `GhostCell` and `GhostToken` are type aliases.

### Deprecated

- `GhostCell` and `GhostToken`.

## [0.2.0]

Released on 2023-12-30.

Yanked on 2024-01-01.

### Changed

- Update the minimum supported Rust version to 1.75.

### Fixed

- Update to `type-factory` 0.1 because 0.2 is [unsound](https://codeberg.org/wackbyte/type-factory/issues/1).

## [0.1.1]

Released on 2023-12-29.

Yanked on 2023-12-30.

### Added

- Research about similar crates to the documentation.

## [0.1.0]

Released on 2023-12-19.

Yanked on 2023-12-30.

### Added

- `GhostCell` and `GhostToken`.
- Export `type-factory` 0.2.
- The minimum supported Rust version is 1.61.

[Unreleased]: https://codeberg.org/wackbyte/toast-cell/compare/v0.3.0...trunk
[0.3.0]: https://codeberg.org/wackbyte/toast-cell/releases/tag/v0.3.0
[0.2.1]: https://codeberg.org/wackbyte/toast-cell/releases/tag/v0.2.1
[0.2.0]: https://codeberg.org/wackbyte/toast-cell/releases/tag/v0.2.0
[0.1.1]: https://codeberg.org/wackbyte/toast-cell/releases/tag/v0.1.1
[0.1.0]: https://codeberg.org/wackbyte/toast-cell/releases/tag/v0.1.0

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
