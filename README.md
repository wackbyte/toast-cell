# `toast-cell`

A type-branded cell for aliasing checked at compile-time.

Many thanks to the [GhostCell] project for pioneering this pattern and to matthieu-m for creating the [`ghost-cell` crate][ghost_cell].

[GhostCell]: https://plv.mpi-sws.org/rustbelt/ghostcell/
[ghost_cell]: https://github.com/matthieu-m/ghost-cell