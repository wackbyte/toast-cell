//! A type-branded cell for aliasing checked at compile-time.
//!
//! It's based on the prior work of [GhostCell], but trades lifetime brands for
//! type brands.
//!
//! [GhostCell]: https://plv.mpi-sws.org/rustbelt/ghostcell/
//!
//! # Usage
//!
//! The interface of this crate is very similar to that of GhostCell. The main
//! difference is that [`Token`]s require a type brand from
//! [`type-factory`](type_factory) instead of a generated lifetime brand.
//!
//! ```
//! use toast_cell::{type_factory, Cell, Token};
//!
//! // 1. Create a brand type.
//! type_factory::with(|brand| {
//!     // 2. Make a unique `Token` out of it.
//!     //    The brand is consumed, so it cannot be used by any other token.
//!     let mut token = Token::new(brand);
//!
//!     // 3. Use it with some `Cell`s.
//!     let cell = Cell::new(0);
//!     let references = [&cell, &cell, &cell];
//!     for cell in references {
//!         *cell.borrow_mut(&mut token) += 1;
//!     }
//!
//!     assert_eq!(cell.into_inner(), 3);
//! });
//! ```
//!
//! As the <code>impl [Unique]</code> brand types generated cannot be copied and
//! are consumed, each `Token` is guaranteed to be unique from any other.
//!
//! The brand types also guarantee that [`Cell`]s used with one token are
//! incompatible with other tokens.
//!
//! ```compile_fail,E0308
//! # use toast_cell::{type_factory, Cell, Token};
//! type_factory::with(|initial| {
//!     let (brand, other_brand) = type_factory::split(initial);
//!     let mut token = Token::new(brand);
//!
//!     let cell = Cell::new(41);
//!     *cell.borrow_mut(&mut token) += 1;       // ✅
//!
//!     let mut other_token = Token::new(other_brand);
//!     *cell.borrow_mut(&mut other_token) -= 1; // ❌
//! });
//! ```
//!
//! # How does it work?
//!
//! The documentation of this crate is currently rather sparse. I would
//! recommend seeing the documentation of
//! [`ghost-cell`](https://crates.io/crates/ghost-cell) for more information on
//! the exact properties of GhostCell.
//!
//! # Safety
//!
//! Unlike GhostCell, this crate has not been formally proven. Use at your own
//! risk.
//!
//! # Minimum supported Rust version
//!
//! The MSRV is currently 1.61.
//!
//! This may change between minor releases.
//!
//! # License
//!
//! I release this crate into the public domain using the
//! [Unlicense](https://unlicense.org/).

// Attributes
#![cfg_attr(not(any(doc, test)), no_std)]
// Lints
#![deny(
	clippy::multiple_unsafe_ops_per_block,
	clippy::undocumented_unsafe_blocks,
	unsafe_op_in_unsafe_fn
)]

use {
	core::{cell::UnsafeCell, marker::PhantomData, mem},
	type_factory::Unique,
};

#[cfg(any(doctest, test))]
mod tests;

pub use type_factory;

pub struct Token<B>
where
	B: Unique,
{
	marker: PhantomData<fn(B) -> B>,
}

impl<B> Token<B>
where
	B: Unique,
{
	pub fn new(#[allow(unused_variables)] brand: B) -> Self {
		Self {
			marker: PhantomData,
		}
	}
}

#[repr(transparent)]
pub struct Cell<T, B>
where
	T: ?Sized,
	B: Unique,
{
	marker: PhantomData<fn(B) -> B>,
	inner: UnsafeCell<T>,
}

impl<T, B> Cell<T, B>
where
	B: Unique,
{
	pub const fn new(value: T) -> Self {
		Self {
			marker: PhantomData,
			inner: UnsafeCell::new(value),
		}
	}

	pub fn into_inner(self) -> T {
		self.inner.into_inner()
	}

	pub fn take(&self, token: &mut Token<B>) -> T
	where
		T: Default,
	{
		mem::take(self.borrow_mut(token))
	}

	pub fn replace(&self, value: T, token: &mut Token<B>) -> T {
		mem::replace(self.borrow_mut(token), value)
	}
}

impl<T, B> Cell<T, B>
where
	T: ?Sized,
	B: Unique,
{
	pub fn from_mut(value: &mut T) -> &mut Self {
		// SAFETY: `Cell<T, B>` is `repr(transparent)` over `T`.
		unsafe { &mut *(value as *mut T as *mut Self) }
	}

	pub fn get_mut(&mut self) -> &mut T {
		self.inner.get_mut()
	}

	pub const fn as_ptr(&self) -> *mut T {
		self.inner.get()
	}

	pub fn borrow<'a>(&'a self, #[allow(unused_variables)] token: &'a Token<B>) -> &'a T {
		// SAFETY: The token is immutably borrowed so there is no aliasing.
		unsafe { &*self.inner.get() }
	}

	pub fn borrow_mut<'a>(
		&'a self,
		#[allow(unused_variables)] token: &'a mut Token<B>,
	) -> &'a mut T {
		// SAFETY: The token is mutably borrowed so there is no aliasing.
		unsafe { &mut *self.inner.get() }
	}
}

impl<T, B> AsMut<T> for Cell<T, B>
where
	T: ?Sized,
	B: Unique,
{
	fn as_mut(&mut self) -> &mut T {
		self.get_mut()
	}
}

impl<T, B> Default for Cell<T, B>
where
	T: Default,
	B: Unique,
{
	fn default() -> Self {
		Self::new(T::default())
	}
}

impl<T, B> From<T> for Cell<T, B>
where
	B: Unique,
{
	fn from(value: T) -> Self {
		Self::new(value)
	}
}

// SAFETY: `Cell<T>` owns `T`, so it can be sent across threads if `T` can.
unsafe impl<T, B> Send for Cell<T, B>
where
	T: ?Sized + Send,
	B: Unique,
{
}

// SAFETY: `Cell<T>` owns `T`, so it can be accessed from different threads
//         if `T` can. `T` must also be `Send` as it can be extracted from
//         methods such as `Cell::replace`
unsafe impl<T, B> Sync for Cell<T, B>
where
	T: ?Sized + Send + Sync,
	B: Unique,
{
}
