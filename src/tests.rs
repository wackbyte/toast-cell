/// ```compile_fail,E0505
/// use toast_cell::{type_factory, Cell, Token};
///
/// type_factory::with(|brand| {
///     let token = Token::new(brand);
///     let cell = Cell::new(1);
///
///     let borrow = cell.borrow(&token);
///     core::mem::drop(token);
///     assert_eq!(*borrow, 1);
/// });
/// ```
fn _cell_borrow_borrows_token() {}

/// ```compile_fail,E0502
/// use toast_cell::{type_factory, Cell, Token};
///
/// type_factory::with(|brand| {
///     let mut token = Token::new(brand);
///     let cell1 = Cell::new(0);
///     let cell2 = Cell::new(2);
///
///     let borrow1 = cell1.borrow_mut(&mut token);
///     let borrow2 = cell2.borrow(&token);
///     assert_eq!(*borrow2, 2);
///
///     *borrow1 += 1;
/// });
/// ```
fn _cell_borrow_mut_borrows_token() {}

/// ```compile_fail,E0505
/// use toast_cell::{type_factory, Cell, Token};
///
/// type_factory::with(|brand| {
///     let token = Token::new(brand);
///     let cell = Cell::new(1);
///
///     let borrow = cell.borrow(&token);
///     core::mem::drop(cell);
///     assert_eq!(*borrow, 1);
/// });
/// ```
fn _cell_borrow_borrows_cell() {}

/// ```compile_fail,E0505
/// use toast_cell::{type_factory, Cell, Token};
///
/// type_factory::with(|brand| {
///     let mut token = Token::new(brand);
///     let cell = Cell::new(0);
///
///     let borrow = cell.borrow_mut(&mut token);
///     core::mem::drop(cell);
///     *borrow += 1;
/// });
/// ```
fn _cell_borrow_mut_borrows_cell() {}

/// ```compile_fail,E0502
/// use toast_cell::{type_factory, Cell, Token};
///
/// type_factory::with(|brand| {
///     let mut value = [1, 2, 3];
///     let cell = Cell::from_mut(&mut value);
///     assert_eq!(value, [1, 2, 3]);
///
///     // Appease the typechecker.
///     let token = Token::new(brand);
///     let _ = cell.borrow(&token);
/// });
/// ```
fn _cell_from_mut_borrows_value() {}
